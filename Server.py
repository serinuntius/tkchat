#coding:utf-8
import socket
class Server:
    def __init__(self,address = "",port = 50007):
        #self.HOST = 'localhost'
        #サーバプログラムを動作させるホストを入力
        self.HOST = address
        self.PORT = port
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.bind(("", self.PORT))#サーバのアドレスをソケットに設定
        self.s.listen(1)#１つの接続要求を待つ
        print "waiting client"
        self.soc, self.addr = self.s.accept()#要求が来るまでブロックする
    def recv(self):
        return self.soc.recv(1024)
    def send(self,message):
        self.soc.send(message)
    def close(self):
        self.soc.close()
"""
HOST = 'localhost'#サーバプログラムを動作させるホストを入力
PORT = 50007#接続するポート番号指定
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))#サーバのアドレスをソケットに設定
s.listen(1)#１つの接続要求を待つ
soc, addr = s.accept()#要求が来るまでブロックする
print 'Conneted by', addr
print 'Go ahead!'#サーバ側から先に書き込む

while 1:
    data = raw_input('(server) > ')#サーバ側の入力
    soc.send(data)#ソケットに書き込む
    data = soc.recv(1024)#1024バイトまでのデータを受け取る
    print '(client) >',data
    if data == "quit":#quitが入力されたら終了
        break
soc.close()
"""
