#coding:utf-8
import socket

class Client:
    def __init__(self,address = "localhost",port = 50007):
        self.HOST = address
        #サーバプログラムを動作させるホストを入力
        self.PORT = port

        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((self.HOST, self.PORT))
    def recv(self):
        return self.s.recv(1024)
    def send(self,message):
        self.s.send(message)
    def close(self):
        self.s.close()

"""
HOST = 'localhost'
#サーバプログラムを動作させるホストを入力
PORT = 50007

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

print 'Wait!'  #サーバ側の書き込みを待つ

while 1:
    data = s.recv(1024)#データの受信（1024バイトまで）
    print '(server) >', data
    data = raw_input('(client) > ')#クライアント側の入力
    s.send(data)#ソケットに書き込む
    if data == "quit":#quitが入力されたら終了
        break
s.close()
"""
