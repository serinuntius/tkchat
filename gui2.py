#coding:utf-8
from Tkinter import *
import Client
import Server
import re
import threading

pat = re.compile("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$")
class Main(threading.Thread):
    def __init__(self):
        self.t = threading.Thread(target = self.clientrecv)
        self.t.setDaemon(True)
        #client serverの変数の用意
        self.c = 0
        self.s = 0

        #TKのセットアップ
        self.root = Tk()
        self.root.option_add('*font', ('FixedSys', 14))

        #IP用のサブウィンドウの変数
        self.sub_win = None
        self.menubar = Menu(self.root)
        self.root.configure(menu = self.menubar)
        self.connect = Menu(self.menubar, tearoff = False)
        self.menubar.add_cascade(label="Menu",underline = 0,menu = self.connect)

        self.connect.add_command(label = "Server",under = 0,command = self.server)


        self.connect.add_command(label = "Connect", under = 0, command = self.connect2)
        self.connect.add_separator()
        self.connect.add_command(label = "Exit",under = 0, command = self.exit)

        # Entryの入力を受け取る為のバッファ
        self.buffer = StringVar()
        self.buffer2 = StringVar()




        self.e = Entry(self.root, textvariable = self.buffer)
        self.e.pack()
        self.e.focus_set()
        self.e.bind('<Return>',self.enter)

        # Listbox の生成
        self.lb = Listbox(self.root)

        # Scrollbar の生成
        self.sb1 = Scrollbar(self.root, orient = 'v', command = self.lb.yview)
        self.sb2 = Scrollbar(self.root, orient = 'h', command = self.lb.xview)

        # Listbox の設定
        self.lb.configure(yscrollcommand = self.sb1.set)
        self.lb.configure(xscrollcommand = self.sb2.set)

        # grid による配置
        self.lb.grid(row = 0, column = 0, sticky = 'nsew')
        self.sb1.grid(row = 0, column = 1, sticky = 'ns')
        self.sb2.grid(row = 1, column = 0, sticky = 'ew')
        self.e.grid(row = 2, columnspan = 2, sticky = 'ew')

        self.root.mainloop()


    def enter(self,event):
        msg = self.buffer.get()
        #msg.encode('utf-8')
        if msg:
            #print str(msg)
            self.buffer.set("")
            self.lb.insert(END,"I say:"+str(msg))
            self.lb.see(END)
            print msg
            if self.c:
                self.c.send(msg)
            elif self.s:
                self.s.send(msg)

    def enter2(self,event):
        global c
        print "return"

        IP = self.buffer2.get()
        if IP:
            test = pat.match(IP)
            if test:
                try:
                    self.root.wm_title("client")
                    self.c = Client.Client(address = IP)
                    self.sub_win.withdraw()
                    self.clientrecv()
                    print "client(IP)"
                except Exception as e:
                    print(e)

            else:
                self.buffer2.set("error IP")

        else:
            try:
                self.root.wm_title("client")
                self.c = Client.Client()
                self.sub_win.withdraw()
                self.t.start()
                print "client()"
            except Exception as e:
                print(e)



    def server(self):
        global s
        self.root.wm_title("server")
        if self.s == 0:
            print "server"
            try:
                self.s = Server.Server()
                self.root.after(10000,self.server)
            except Exception as e:
                print e
        else:
            print "server else"
            try:
                msg = self.s.recv()
            except Exception as e:
                print e
            if msg != "":
                self.lb.insert(END,"Friend says:"+str(msg))
                self.b.see(END)
                print msg
                msg = ""

            self.root.after(10000,server)




    def connect2(self):
        global sub_win
        if self.sub_win is None or not self.sub_win.winfo_exists():
            self.sub_win = Toplevel()
            self.sub_win.title("Type IP Adress")
            self.e2 = Entry(self.sub_win, textvariable = self.buffer2)
            self.e2.pack()
            self.e2.focus_set()
            self.e2.bind("<Return>",self.enter2)
        else:
            self.sub_win.deiconify()



    def exit(self):
        sys.exit()

    def clientrecv(self):
        try:
            msg = self.c.recv()
        except Exception as e:
            print e
        if msg != "":
                self.lb.insert(END,"Friend says:"+str(msg))
                self.lb.see(END)
                print msg
                msg = ""
        self.connect.after(10000,self.clientrecv)

app = Main()

"""
c = 0
s = 0

root = Tk()
root.option_add('*font', ('FixedSys', 14))
sub_win = None

def enter(event):
    msg = buffer.get()
    #msg.encode('utf-8')
    if msg:
        #print str(msg)
        buffer.set("")
        lb.insert(END,"I say:"+str(msg))
        lb.see(END)
        print msg
        if c:
            c.send(msg)
        elif s:
            s.send(msg)
def enter2(event):
    global c
    print "return"

    IP = buffer2.get()
    if IP:
        test = pat.match(IP)
        if test:
            try:
                root.wm_title("client")
                c = Client.Client(address = IP)
                sub_win.withdraw()
                clientrecv()
                print "client(IP)"
            except Exception as e:
                print(e)

        else:
            buffer2.set("error IP")

    else:
        try:
            root.wm_title("client")
            c = Client.Client()
            sub_win.withdraw()
            t.start()
            print "client()"
        except Exception as e:
            print(e)



def server():
    global s
    root.wm_title("server")
    if s == 0:
        print "server"
        try:
            s = Server.Server()
            root.after(10000,server)
        except Exception as e:
            print e
    else:
        print "server else"
        try:
            msg = s.recv()
        except Exception as e:
            print e
        if msg != "":
            lb.insert(END,"Friend says:"+str(msg))
            lb.see(END)
            print msg
            msg = ""

        root.after(10000,server)




def connect2():
    global sub_win
    if sub_win is None or not sub_win.winfo_exists():
        sub_win = Toplevel()
        sub_win.title("Type IP Adress")
        e2 = Entry(sub_win, textvariable = buffer2)
        e2.pack()
        e2.focus_set()
        e2.bind("<Return>",enter2)
    else:
        sub_win.deiconify()



def exit():
    sys.exit()

def clientrecv():
    try:
        msg = c.recv()
    except Exception as e:
        print e
    if msg != "":
            lb.insert(END,"Friend says:"+str(msg))
            lb.see(END)
            print msg
            msg = ""
    connect.after(1,clientrecv)


t = threading.Thread(target = clientrecv)
t.setDaemon(True)


#メニューバーの設定
menubar = Menu(root)
root.configure(menu = menubar)
connect = Menu(menubar, tearoff = False)
menubar.add_cascade(label="Menu",underline = 0,menu = connect)

connect.add_command(label = "Server",under = 0,command = server)


connect.add_command(label = "Connect", under = 0, command = connect2)
connect.add_separator()
connect.add_command(label = "Exit",under = 0, command = exit)

# 式を格納するオブジェクト
buffer = StringVar()
buffer2 = StringVar()




e = Entry(root, textvariable = buffer)
e.pack()
e.focus_set()
e.bind('<Return>',enter)

# Listbox の生成
lb = Listbox(root)

# Scrollbar の生成
sb1 = Scrollbar(root, orient = 'v', command = lb.yview)
sb2 = Scrollbar(root, orient = 'h', command = lb.xview)

# Listbox の設定
lb.configure(yscrollcommand = sb1.set)
lb.configure(xscrollcommand = sb2.set)

# grid による配置
lb.grid(row = 0, column = 0, sticky = 'nsew')
sb1.grid(row = 0, column = 1, sticky = 'ns')
sb2.grid(row = 1, column = 0, sticky = 'ew')
e.grid(row = 2, columnspan = 2, sticky = 'ew')

root.mainloop()
"""
